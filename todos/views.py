from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {"lists": lists}
    return render(request, "todo_lists/list.html", context)


def todo_list_detail(request, id):
    detail = get_object_or_404(TodoList, id=id)
    name = TodoList.objects.get(id=id).name
    context = {
        "detail": detail,
        "name": name,
    }
    return render(request, "todo_lists/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todo_lists/create.html", context)


def todo_list_update(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todolist.id)
    else:
        form = TodoListForm(instance=todolist)

    context = {"form": form, "todolist": todolist}

    return render(request, "todo_lists/edit.html", context)


def todo_list_delete(request, id):
    todelete = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todelete.delete()
        return redirect("todo_list_list")

    return render(request, "todo_lists/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todo_lists/create.html", context)


def todo_item_update(request, id):
    todoitem = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todoitem)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todoitem.id)
    else:
        form = TodoItemForm(instance=todoitem)

    context = {"form": form, "todoitem": todoitem}

    return render(request, "todo_lists/edit.html", context)
